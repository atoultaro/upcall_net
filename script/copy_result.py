# -*- coding: utf-8 -*-
"""
Copy the files of accuracy performance numbers such as true positive, false 
positive and false negative to a new place, in order to share them easily 
without including the big fat trained model files.

Created on Mon Nov 12 14:55:11 2018

@author: ys587
"""
import os, glob
from shutil import copyfile

result_folder = r'/home/ys587/__ExptResult/__V4_Paper'
result_only_folder = r'/home/ys587/__ExptResult/__V4_Paper/__result_only'
model_folder_list = sorted(glob.glob(os.path.join(result_folder+'/','cv*')))

for mm in model_folder_list:
    #mm1 = mm+'_result_only'
    mm1 = os.path.join(result_only_folder, os.path.split(mm)[-1])
    os.makedirs(mm1, exist_ok=True)
    
    file_list = sorted(glob.glob(os.path.join(mm,'*.txt')))
    for ff in file_list:
        copyfile(ff, os.path.join(mm1, os.path.basename(ff)))
    
    os.makedirs(os.path.join(mm1, '__full_data'), exist_ok=True)
    file_list = sorted(glob.glob(os.path.join(mm, '__full_data','*.txt')))
    for ff in file_list:
        copyfile(ff, os.path.join(mm1, '__full_data',os.path.basename(ff)))
        
    os.makedirs(os.path.join(mm1, '__split_data'), exist_ok=True)
    file_list = sorted(glob.glob(os.path.join(mm, '__split_data','*.txt')))
    for ff in file_list:
        copyfile(ff, os.path.join(mm1, '__split_data',os.path.basename(ff)))

        
